#!/usr/bin/env python
#
# Equivalent of:
#   openssl req -new -x509 -key key.pem -out cert.pem

import sys
import datetime

# backend
from cryptography.hazmat.backends import default_backend
# serialization
from cryptography.hazmat.primitives import serialization
# self-signed
from cryptography import x509
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives import hashes

USAGE="{0} new_cert_file [key_file]".format(sys.argv[0])

KEY = 'key.pem'
PASSWORD = None

IDENTITY = 'Development CA X1'
DAYS = 30

SUBJECT = x509.Name([
                     x509.NameAttribute(NameOID.COUNTRY_NAME, 'CZ'),
                     x509.NameAttribute(NameOID.LOCALITY_NAME, 'Prague'),
                     x509.NameAttribute(NameOID.ORGANIZATION_NAME, 'CZ.NIC, z.s.p.o.'),
                     x509.NameAttribute(NameOID.ORGANIZATIONAL_UNIT_NAME, 'Turris'),
                     x509.NameAttribute(NameOID.COMMON_NAME, IDENTITY),
                   ])


if len(sys.argv) == 2:
    cert_file = sys.argv[1]
    key_file = KEY
elif len(sys.argv) == 3:
    cert_file = sys.argv[1]
    key_file = sys.argv[2]
else:
    print(USAGE)
    sys.exit(1)



with open(key_file, 'rb') as f:
    private_key = serialization.load_pem_private_key(
                                                     data=f.read(),
                                                     password=PASSWORD,
                                                     backend=default_backend()
                                                    )

# dates
not_before = datetime.datetime.utcnow()
not_after = datetime.datetime.utcnow() + datetime.timedelta(days=DAYS)
serial_number = x509.random_serial_number()

# Generate self-signed certificate
cert = x509.CertificateBuilder(
                               issuer_name=SUBJECT,
                               subject_name=SUBJECT,
                               public_key=private_key.public_key(),
                               serial_number=serial_number,
                               not_valid_before=not_before,
                               not_valid_after=not_after,
                              )
# key identifiers
ski = x509.SubjectKeyIdentifier.from_public_key(private_key.public_key())
cert = cert.add_extension(
                          ski,
                          critical=False
                         )
cert = cert.add_extension(
                          x509.AuthorityKeyIdentifier(
                                                      ski.digest,
                                                      authority_cert_issuer=None,
                                                      authority_cert_serial_number=None
                                                     ),
                          critical=False
                         )
# critical, CA:true
cert = cert.add_extension(
                          x509.BasicConstraints(
                                                ca=True,
                                                path_length=None
                                               ),
                          critical=True
                         )
# "Certificate Sign" and "CRL Sign" key usage
cert = cert.add_extension(
                          x509.KeyUsage(
                                        digital_signature=False,
                                        content_commitment=False,
                                        key_encipherment=False,
                                        data_encipherment=False,
                                        key_agreement=False,
                                        key_cert_sign=True,
                                        crl_sign=True,
                                        encipher_only=False,
                                        decipher_only=False
                                       ),
                          critical=False
                         )
cert = cert.sign(private_key, hashes.SHA256(), default_backend())

# Write cert to disk.
with open(cert_file, 'wb') as f:
    f.write(cert.public_bytes(serialization.Encoding.PEM))
