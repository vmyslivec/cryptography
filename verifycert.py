#!/usr/bin/env python
#
# Equivalent of:
#   openssl verify -no-CApath -CAfile cacert.pem cert.pem
#
# The script checks:
#   - CommonName of the cert
#   - Allowed signature algorithms
#   - Expiration dates
#   - Valid issuer' signature
#   - Unknown critical extensions
#   - Issuer name and CA Subject
#   - Certificate Authority Key Identifier and CA Subject Key Identifier
#   - CA Basic Constraints and Key Usage
#   - Unknown critical extensions

import sys
import datetime

# backend
from cryptography.hazmat.backends import default_backend
# verify cert
from cryptography import x509,exceptions
from cryptography.x509 import ExtensionOID
from cryptography.x509.oid import ExtendedKeyUsageOID
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import ec, rsa, padding

USAGE="""{0} cacert_file cert_file identity usage_type

    usage_type      'client', 'server' or 'ignore'""".format(sys.argv[0])

PASSWORD = None

DAYS = 10

ALLOWED_HASHES = {
    hashes.SHA224,
    hashes.SHA256,
    hashes.SHA384,
    hashes.SHA512,
}


if len(sys.argv) == 5:
    cacert_file = sys.argv[1]
    cert_file = sys.argv[2]
    identity = sys.argv[3]
    usage_type = sys.argv[4]
else:
    print(USAGE)
    sys.exit(1)

if usage_type not in {"client", "server", "ignore"}:
    print("Usage type must be 'client', 'server' or 'ignore'", file=sys.stderr)
    sys.exit(1)


with open(cacert_file, 'rb') as f:
    ca_cert = x509.load_pem_x509_certificate(
                                             data=f.read(),
                                             backend=default_backend()
                                            )

with open(cert_file, 'rb') as f:
    cert = x509.load_pem_x509_certificate(
                                          data=f.read(),
                                          backend=default_backend()
                                         )


# Check signature hash algorithm
def check_hash(cert):
    allowed = False
    signed_hash = cert.signature_hash_algorithm
    for allowed_hash in ALLOWED_HASHES:
        if isinstance(signed_hash, allowed_hash):
            allowed = True
            break

    return allowed

def check_common(cert):
    if not check_hash(cert):
        print("check_common: Certificate is signed with not allowed hash ({})".format(signed_hash.name), file=sys.stderr)
        return 1

    if now < cert.not_valid_before:
        print("check_common: Certificate is not valid yet", file=sys.stderr)
        return 2

    if cert.not_valid_after < now:
        print("check_common: Certificate is expired", file=sys.stderr)
        return 3

    return 0


#now = datetime.datetime.utcnow() + datetime.timedelta(days=90)
now = datetime.datetime.utcnow()

# check the cert ===========================================
# CommonName -------------------------------------
common_names = cert.subject.get_attributes_for_oid(x509.NameOID.COMMON_NAME)
if len(common_names) != 1:
    print("Certificate has {} CommonNames".format(len(common_names)), file=sys.stderr)
    exit(65)

common_name = common_names[0].value
if common_name != identity:
    print("Certificate CommonName ({}) does not match with identity provided ({})".format(common_name, identity), file=sys.stderr)
    exit(65)

# hash, dates ------------------------------------
ret = check_common(cert)
if ret != 0:
    print("Certificate is not valid", file=sys.stderr)
    exit(65+ret)

# issuer -----------------------------------------
issuer = cert.issuer
ca = ca_cert.subject

if issuer != ca:
    print("The cert issuer is not a CA provided", file=sys.stderr)
    exit(69)

# signature --------------------------------------
public_key = ca_cert.public_key()
signature = cert.signature
data = cert.tbs_certificate_bytes
signature_algorithm = cert.signature_hash_algorithm

try:
    if isinstance(public_key, rsa.RSAPublicKey):
        public_key.verify(
                          signature,
                          data,
                          padding.PKCS1v15(),
                          signature_algorithm
                         )
    elif isinstance(public_key, ec.EllipticCurvePublicKey):
        public_key.verify(
                          signature,
                          data,
                          ec.ECDSA(signature_algorithm)
                         )
    else:
        print('CA public key type is not supported', file=sys.stderr)
        exit(70)

except exceptions.InvalidSignature:
    print("Certificate signature is not valid", file=sys.stderr)
    exit(71)

# extensions -------------------------------------
# look for critical extensions if we don't understand some
aki_ext = None
extended_key_usage_ext = None
for ext in cert.extensions:
    # ignore Basic Constraints, Key Usage and Subject Key Identifier
    if ext.oid in {
                   ExtensionOID.BASIC_CONSTRAINTS,
                   ExtensionOID.KEY_USAGE,
                   ExtensionOID.SUBJECT_KEY_IDENTIFIER,
                  }:
        continue

    # set Authority Key Identifier (will be processed later)
    if ext.oid == ExtensionOID.AUTHORITY_KEY_IDENTIFIER:
        aki_ext = ext
        continue

    # set Extended Key Usage (will be processed later)
    if ext.oid == ExtensionOID.EXTENDED_KEY_USAGE:
        extended_key_usage_ext = ext
        continue

    # unknown otherwise: fail if critical
    if ext.critical:
        print("Unknown critical extension with oid {}".format(ext.oid.dotted_string), file=sys.stderr)
        exit(72)
    else:
        print("warning: Unknown extension with oid {}".format(ext.oid.dotted_string), file=sys.stderr)


# key identifiers --------------------------------
# if Authority Key Identifier was not among the extensions
if not aki_ext:
    print("Authority Key Identifier in cert is missing", file=sys.stderr)
    exit(73)

try:
    ca_ski_ext = ca_cert.extensions.get_extension_for_class(x509.SubjectKeyIdentifier)

except x509.ExtensionNotFound:
    print("Subject Key Identifier in CA cert is missing", file=sys.stderr)
    exit(74)

ca_aki = x509.AuthorityKeyIdentifier.from_issuer_subject_key_identifier(ca_ski_ext)

if aki_ext.value != ca_aki:
    print("Authority Key Identifier does not correspond to CA", file=sys.stderr)
    exit(75)

# extended key usage -----------------------------
if usage_type != "ignore":
    if not extended_key_usage_ext:
        print("Extended Key Usage in cert is missing", file=sys.stderr)
        exit(76)

    extended_key_usage = extended_key_usage_ext.value
    if usage_type == "server" and ExtendedKeyUsageOID.SERVER_AUTH not in extended_key_usage:
        print("Required 'Server Auth' is not present in certificate Extended Key Usage", file=sys.stderr)
        exit(76)
    if usage_type == "client" and ExtendedKeyUsageOID.CLIENT_AUTH not in extended_key_usage:
        print("Required 'Client Auth' is not present in certificate Extended Key Usage", file=sys.stderr)
        exit(76)


# check the ca cert ========================================
# hash, dates ------------------------------------
ret = check_common(ca_cert)
if ret != 0:
    print("CA certificate is not valid", file=sys.stderr)
    exit(97+ret)

# extensions -------------------------------------
# look for critical extensions if we don't understand some
basic_constraints_ext = None
key_usage_ext = None
for ext in ca_cert.extensions:
    # ignore Authority and Subject Key Identifier and Extended Key Usage
    if ext.oid in {
                   ExtensionOID.AUTHORITY_KEY_IDENTIFIER,
                   ExtensionOID.SUBJECT_KEY_IDENTIFIER,
                   ExtensionOID.EXTENDED_KEY_USAGE,
                  }:
        continue

    # set Basic Constraints (will be processed later)
    if ext.oid == ExtensionOID.BASIC_CONSTRAINTS:
        basic_constraints_ext = ext
        continue

    # set Key Usage (will be processed later)
    if ext.oid == ExtensionOID.KEY_USAGE:
        key_usage_ext = ext
        continue

    # unknown otherwise: fail if critical
    if ext.critical:
        print("Unknown ca critical extension with oid {}".format(ext.oid.dotted_string), file=sys.stderr)
        exit(72)
    else:
        print("warning: Unknown ca extension with oid {}".format(ext.oid.dotted_string), file=sys.stderr)

# basic constraints ------------------------------
# if Basic Constraints was not among the extensions
if not basic_constraints_ext:
    print("Basic constraints in CA is missing", file=sys.stderr)
    exit(101)

if not basic_constraints_ext.value.ca:
    print("Issuer is not a CA", file=sys.stderr)
    exit(101)

# key usage --------------------------------------
# if Key Usage was not among the extensions
if not key_usage_ext:
    print("Key usage in CA is missing", file=sys.stderr)
    exit(102)

if not key_usage_ext.value.key_cert_sign:
    print("CA key usage does not allow cert signing", file=sys.stderr)
    exit(102)
