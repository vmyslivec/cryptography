#!/usr/bin/env python
#
# Equivalent of:
#   openssl ca -batch -keyfile key.pem -in req.pem -out cert.pem

import sys
import datetime

# backend
from cryptography.hazmat.backends import default_backend
# serialization
from cryptography.hazmat.primitives import serialization
# signing cert
from cryptography import x509
from cryptography.x509.oid import ExtendedKeyUsageOID
from cryptography.hazmat.primitives import hashes

USAGE="""{0} key_file cacert_file req_file usage_type new_cert_file

    usage_type      'client', 'server' or 'none'""".format(sys.argv[0])

PASSWORD = None

DAYS = 10

ALLOWED_HASHES = {
    hashes.SHA224,
    hashes.SHA256,
    hashes.SHA384,
    hashes.SHA512,
}

if len(sys.argv) == 6:
    key_file = sys.argv[1]
    cacert_file = sys.argv[2]
    req_file = sys.argv[3]
    usage_type = sys.argv[4]
    cert_file = sys.argv[5]
else:
    print(USAGE)
    sys.exit(1)

if usage_type not in {"client", "server", "none"}:
    print("Usage type must be 'client', 'server' or 'none'", file=sys.stderr)
    sys.exit(1)


with open(key_file, 'rb') as f:
    private_key = serialization.load_pem_private_key(
                                                     data=f.read(),
                                                     password=PASSWORD,
                                                     backend=default_backend()
                                                    )

with open(req_file, 'rb') as f:
    request = x509.load_pem_x509_csr(
                                     data=f.read(),
                                     backend=default_backend()
                                    )

with open(cacert_file, 'rb') as f:
    ca_cert = x509.load_pem_x509_certificate(
                                             data=f.read(),
                                             backend=default_backend()
                                            )


# Check signature hash algorithm
allowed = False
signed_hash = request.signature_hash_algorithm
for allowed_hash in ALLOWED_HASHES:
    if isinstance(signed_hash, allowed_hash):
        allowed = True
        break

if not allowed:
    print("Request is signed with not allowed hash ({})".format(signed_hash.name), file=sys.stderr)
    exit(20)

# Check signature itself
if not request.is_signature_valid:
    print("Request signature is not valid", file=sys.stderr)
    exit(21)

# dates
not_before = datetime.datetime.utcnow()
not_after = datetime.datetime.utcnow() + datetime.timedelta(days=DAYS)
# TODO check existence
serial_number = x509.random_serial_number()

# Issue the certificate (sign a request)
cert = x509.CertificateBuilder(
                               issuer_name=ca_cert.subject,
                               subject_name=request.subject,
                               public_key=request.public_key(),
                               serial_number=serial_number,
                               not_valid_before=not_before,
                               not_valid_after=not_after,
                              )
# key identifiers
cert = cert.add_extension(
                          x509.SubjectKeyIdentifier.from_public_key(request.public_key()),
                          critical=False
                         )
ca_ski = ca_cert.extensions.get_extension_for_class(x509.SubjectKeyIdentifier)
cert = cert.add_extension(
                          x509.AuthorityKeyIdentifier.from_issuer_subject_key_identifier(ca_ski),
                          critical=False
                         )
# critical, CA:false
cert = cert.add_extension(
                          x509.BasicConstraints(
                                                ca=False,
                                                path_length=None
                                               ),
                          critical=True
                         )
# "Digital Signature" (maybe "Non Repudiation" and "Key Encipherment") key usage
cert = cert.add_extension(
                          x509.KeyUsage(
                                        digital_signature=True,
                                        content_commitment=False,
                                        key_encipherment=False,
                                        data_encipherment=False,
                                        key_agreement=False,
                                        key_cert_sign=False,
                                        crl_sign=False,
                                        encipher_only=False,
                                        decipher_only=False
                                       ),
                          critical=False
                         )

# Extended Key Usage
if   usage_type == "client":
    extended_key_usage = [ExtendedKeyUsageOID.CLIENT_AUTH]
elif usage_type == "server":
    extended_key_usage = [ExtendedKeyUsageOID.SERVER_AUTH]
else:
    extended_key_usage = []

if extended_key_usage:
    cert = cert.add_extension(
                              x509.ExtendedKeyUsage(extended_key_usage),
                              critical=False
                             )

# Sign the certificate
cert = cert.sign(private_key, hashes.SHA256(), default_backend())

# Write cert to disk.
with open(cert_file, 'wb') as f:
    f.write(cert.public_bytes(serialization.Encoding.PEM))
