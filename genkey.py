#!/usr/bin/env python
#
# Equivalent of:
#   openssl ecparam -genkey -name prime256v1 -out ecdsa_key.pem
#   openssl genrsa -out rsa_key.pem 4096

import sys

# backend
from cryptography.hazmat.backends import default_backend
# serialization
from cryptography.hazmat.primitives import serialization
# private/public keys
from cryptography.hazmat.primitives.asymmetric import ec, rsa

USAGE = """
{0} new_key_file [key_type]

    key_type    ECDSA (default) or RSA
""".format(sys.argv[0])

TYPE_RSA = 'rsa'
TYPE_ECDSA = 'ecdsa'

PASSWORD = None

if len(sys.argv) == 2:
    key_file = sys.argv[1]
    key_type = TYPE_ECDSA
elif len(sys.argv) == 3:
    key_file = sys.argv[1]
    key_type = sys.argv[2].lower()
else:
    print(USAGE)
    sys.exit(1)


# key generation
if key_type == TYPE_RSA:
    size = 4096 # bits
    exponent = 0x10001 # 65537
    private_key = rsa.generate_private_key(
                                           public_exponent=exponent,
                                           key_size=size,
                                           backend=default_backend()
                                          )
elif key_type == TYPE_ECDSA:
    curve = ec.SECP256R1()
    private_key = ec.generate_private_key(
                                          curve=curve,
                                          backend=default_backend()
                                         )
else:
    print("Unsupported key type")
    sys.exit(1)



# Write key to disk.
with open(key_file, 'wb') as f:
    key_bytes = private_key.private_bytes(
                                          encoding=serialization.Encoding.PEM,
                                          format=serialization.PrivateFormat.TraditionalOpenSSL,
                                          encryption_algorithm=serialization.NoEncryption()
                                         )
    f.write(key_bytes)
