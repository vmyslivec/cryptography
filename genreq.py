#!/usr/bin/env python
#
# Equivalent of:
#   openssl req -new -key key.pem -out csr.pem

import sys

# backend
from cryptography.hazmat.backends import default_backend
# serialization
from cryptography.hazmat.primitives import serialization
# request
from cryptography import x509
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives import hashes

USAGE="{0} key_file identity new_csr_file".format(sys.argv[0])

PASSWORD = None

if len(sys.argv) == 4:
    key_file = sys.argv[1]
    identity = sys.argv[2]
    csr_file = sys.argv[3]
else:
    print(USAGE)
    sys.exit(1)

# minimal
SUBJECT = x509.Name([
                     x509.NameAttribute(NameOID.COMMON_NAME, identity),
                   ])
# full
#SUBJECT = x509.Name([
#                     x509.NameAttribute(NameOID.COUNTRY_NAME, 'CZ'),
#                     x509.NameAttribute(NameOID.LOCALITY_NAME, 'Prague'),
#                     x509.NameAttribute(NameOID.ORGANIZATION_NAME, 'CZ.NIC, z.s.p.o.'),
#                     x509.NameAttribute(NameOID.ORGANIZATIONAL_UNIT_NAME, 'Turris'),
#                     x509.NameAttribute(NameOID.COMMON_NAME, identity),
#                   ])




with open(key_file, 'rb') as f:
    private_key = serialization.load_pem_private_key(
                                                     data=f.read(),
                                                     password=PASSWORD,
                                                     backend=default_backend()
                                                    )

# Generate a signed CSR
csr = x509.CertificateSigningRequestBuilder(subject_name=SUBJECT)
csr = csr.sign(private_key, hashes.SHA256(), default_backend())

# Write CSR to disk.
with open(csr_file, 'wb') as f:
    f.write(csr.public_bytes(serialization.Encoding.PEM))
