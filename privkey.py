#!/usr/bin/env python
#
# Equivalent of:
#   openssl ec  -noout -text -in ecdsa_key.pem
#   openssl rsa -noout -text -in rsa_key.pem

import sys

# backend
from cryptography.hazmat.backends import default_backend
# serialization
from cryptography.hazmat.primitives import serialization
# private/public keys
from cryptography.hazmat.primitives.asymmetric import ec, rsa

USAGE = "{0} [key_file]".format(sys.argv[0])

KEY = 'key.pem'
PASSWORD = None

if len(sys.argv) == 1:
    key_file = KEY
elif len(sys.argv) == 2:
    key_file = sys.argv[1]
else:
    print(USAGE)
    sys.exit(1)



with open(key_file, "rb") as f:
    private_key = serialization.load_pem_private_key(
                                                     data=f.read(),
                                                     password=PASSWORD,
                                                     backend=default_backend()
                                                    )

public_key = private_key.public_key()
if isinstance(public_key, rsa.RSAPublicKey):
    print('{0} bit RSA key'.format(public_key.key_size))
elif isinstance(public_key, ec.EllipticCurvePublicKey):
    print('{0} bit ECDSA key over {1} curve'.format(public_key.key_size, public_key.curve.name))
else:
    print('Key type is unsupported')
